#!/usr/bin/wish

set nrOfElves 1000
set caloriesPerElf [list]
set realNrOfElves 0

proc listFromFile {filename} {
    set f [open $filename r]
    set data [split [string trim [read $f]]]
    close $f
    return $data
}


proc assignCaloriesToElves {} {
    global calories caloriesPerElf realNrOfElves
    set currentElf 0
    for {set i 0} {$i < [llength $calories]} {incr i} {
	if { [string is integer -strict [lindex $calories $i] ] != 1 } {
	    incr currentElf
	    set previousCaloriesPerElf 0
 	} else {
	    set previousCaloriesPerElf  [lindex $caloriesPerElf $currentElf]
	    if { [string is integer -strict $previousCaloriesPerElf ] != 1 } {
		set previousCaloriesPerElf 0
	    }
	    lset caloriesPerElf $currentElf [expr ([lindex $calories $i] + $previousCaloriesPerElf)]
	}
}
    set realNrOfElves [expr ($currentElf + 1)]
    puts "Real Nr of Elves=$realNrOfElves (0..$currentElf)"
}


proc findTheElfWithMostCalories {nr} {
    global caloriesPerElf
    set bestElf 0
    set currentMax 0
    for {set i 0} {$i < $nr} {incr i} {
	if { [lindex $caloriesPerElf $i] > $currentMax } {
	    set bestElf $i
	    set currentMax  [lindex $caloriesPerElf $i]
	}
    }
    return $currentMax 
}


proc findTopThreeElves {nr} {
    global caloriesPerElf
    set caloriesPerElfSorted [lreverse [lsort -integer $caloriesPerElf]]
    set topThree [lrange $caloriesPerElfSorted 0 2]
    set sumTopThree 0
    for {set i 0} {$i < [llength $topThree]} {incr i} {
	set sumTopThree [expr $sumTopThree + [lindex $topThree $i]]
    }
    return $sumTopThree
}

### main ###

# display the file
# puts [listFromFile filelist.txt]

set calories [listFromFile filelist.txt]
assignCaloriesToElves
puts "Task #1 result: [findTheElfWithMostCalories $realNrOfElves]"
puts "Task #2 result: [findTopThreeElves $realNrOfElves]"

## 01.tcl ends here ##
